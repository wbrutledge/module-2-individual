<!DOCTYPE html>

<head>
	<title>Calculator</title>
	<link rel="stylesheet" href="calcstyle.css" type="text/css" media="screen">
	<meta charset="UTF-8">
</head>
<body>
	
	<div id="container">
		<form action="calculator.php" method="GET">
			<div id="innerContain">
				<h1>Basic Calculator</h1>
			
			<div class="inputForm">
				<label for="firstNumInput"></label>
				<input type="text" name="firstNumInput" id="firstNumInput" value="0"/>
			</div>
			
			<div id="functionSelect">
				<input type="radio" name="func" value="add" id="addInput" /> <label for="addInput">	&#43;</label><br>
				<input type="radio" name="func" value="subtract" id="subtractInput" /> <label for="subtractInput">-</label><br>
				<input type="radio" name="func" value="multiply" id="multiplyInput" /> <label for="multiplyInput">*</label><br>
				<input type="radio" name="func" value="divide" id="divideInput" /> <label for="divideInput">&#247;</label>
			</div>
			
			<div class="inputForm">
				<label for="lastNumInput"></label>
				<input type="text" name="lastNumInput" id="lastNumInput" value="0"/>
			</div>
		</div>

			<div id="calculate">
				<input type="submit" value="Calculate" />
			</div>
			<div id="clear">
				<input type="reset" value="Clear"/>
			</div>

<?php
		@$num1 = (float) $_GET['firstNumInput'];
		@$num2 = (float) $_GET['lastNumInput'];
		@$func = $_GET['func'];
		$result = NULL;
		switch ($func) {
			case "add":
				$result=$num1+$num2;
			break;
			case "subtract":
				$result=$num1-$num2;
			break;
			case "multiply":
				$result=$num1*$num2;
			break;
			case "divide":
				if ($num2!=0) {
					$result=$num1/$num2;
				}
				else {
					$result="Error";}
				break;
				
			default:
				break;
		}
		

		

?>
		
		
		
			<div id="result">
				<input type="text" name="result" id="calcResult" value="<?php print $result;?>">
			</div>
		</form>
		
		
	</div>
</body>

</html>